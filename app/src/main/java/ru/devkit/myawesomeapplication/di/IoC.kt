package ru.devkit.myawesomeapplication.di

import ru.devkit.myawesomeapplication.di.strategy.DependencyStrategy

object IoC {

    private val dependencies = mutableMapOf<String, DependencyStrategy>()

    @Suppress("UNCHECKED_CAST")
    fun <T> resolve(key: String, vararg args: Any): T {
        return dependencies[key]?.provide(args) as? T
            ?: throw IllegalArgumentException("Dependency \"$key\" was not found")
    }

    fun register(key: String, value: DependencyStrategy) {
        dependencies[key] = value
    }
}