package ru.devkit.myawesomeapplication.di.strategy

class NewInstanceStrategy(
    private val provider: (Array<out Any>) -> Any
) : DependencyStrategy {
    override fun provide(args: Array<out Any>) = provider.invoke(args)
}