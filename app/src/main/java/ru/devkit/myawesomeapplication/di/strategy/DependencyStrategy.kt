package ru.devkit.myawesomeapplication.di.strategy

interface DependencyStrategy {
    fun provide(args: Array<out Any>): Any
}