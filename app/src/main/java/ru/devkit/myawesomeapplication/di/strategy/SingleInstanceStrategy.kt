package ru.devkit.myawesomeapplication.di.strategy

class SingleInstanceStrategy(
    private val provider: (Array<out Any>) -> Any
) : DependencyStrategy {

    private var dependency: Any? = null

    override fun provide(args: Array<out Any>): Any {
        return dependency ?: provider.invoke(args).also { dependency = it }
    }
}