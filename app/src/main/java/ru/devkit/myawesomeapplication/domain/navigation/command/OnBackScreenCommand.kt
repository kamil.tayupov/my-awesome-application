package ru.devkit.myawesomeapplication.domain.navigation.command

import android.app.Activity
import ru.devkit.myawesomeapplication.data.source.api.PortfolioApi
import ru.devkit.myawesomeapplication.data.source.api.StocksApi
import ru.devkit.myawesomeapplication.di.IoC
import ru.devkit.myawesomeapplication.domain.command.ICommand
import ru.devkit.myawesomeapplication.domain.navigation.ScreenStack

class OnBackScreenCommand : ICommand {
    override fun execute() {
        val stack = IoC.resolve<ScreenStack>("ScreenStack")
        stack.pop()
        if (stack.isEmpty()) {
            IoC.resolve<PortfolioApi>("PortfolioApi").release()
            IoC.resolve<StocksApi>("StocksApi").release()
            IoC.resolve<Activity>("Activity").finish()
        } else {
            OpenCurrentScreenCommand().execute()
        }
    }
}