package ru.devkit.myawesomeapplication.domain.command

interface ICommand {
    fun execute()
}