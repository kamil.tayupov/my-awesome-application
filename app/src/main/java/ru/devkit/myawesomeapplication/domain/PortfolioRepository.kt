package ru.devkit.myawesomeapplication.domain

import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import ru.devkit.myawesomeapplication.data.model.InvestmentDto
import ru.devkit.myawesomeapplication.data.source.PortfolioLocalDataSource
import ru.devkit.myawesomeapplication.data.source.PortfolioRemoteDataSource
import ru.devkit.myawesomeapplication.data.source.StockDataSource
import ru.devkit.myawesomeapplication.di.IoC
import ru.devkit.myawesomeapplication.domain.model.Investment
import ru.devkit.myawesomeapplication.domain.model.Portfolio

class PortfolioRepository {

    private val localDataSource = IoC.resolve<PortfolioLocalDataSource>("PortfolioLocalDataSource")
    private val remoteDataSource = IoC.resolve<PortfolioRemoteDataSource>("PortfolioRemoteDataSource")
    private val stockDataSource = IoC.resolve<StockDataSource>("StockDataSource")

    val data = remoteDataSource.getData()
        .map { data ->
            val investments = data.items.map { it.toInvestment() }
            val portfolio = Portfolio(investments)
            localDataSource.setData(portfolio)
            portfolio
        }
        .onStart { emit(localDataSource.getData()) }

    private fun InvestmentDto.toInvestment(): Investment {
        val stock = stockDataSource.getStock(id)
        return Investment(
            id = id,
            name = stock.name,
            quantity = quantity,
            currentPrice = stock.currentPrice,
            firstPrice = initialPrice,
            lastPrice = stock.lastPrice
        )
    }
}