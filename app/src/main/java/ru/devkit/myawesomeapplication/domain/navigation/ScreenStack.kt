package ru.devkit.myawesomeapplication.domain.navigation

import java.util.*

class ScreenStack {

    private val stack = Stack<Screen>()

    fun currentScreen(): Screen = stack.peek()

    fun push(screen: Screen) = stack.push(screen)

    fun pop(): Screen = stack.pop()

    fun isEmpty(): Boolean = stack.isEmpty()
}