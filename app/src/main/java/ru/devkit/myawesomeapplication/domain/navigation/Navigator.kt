package ru.devkit.myawesomeapplication.domain.navigation

import androidx.fragment.app.FragmentManager
import ru.devkit.myawesomeapplication.di.IoC
import ru.devkit.myawesomeapplication.presentation.ScreenFragmentFactory

class Navigator(
    private val fragmentManager: FragmentManager,
    private val containerId: Int
) {

    private val factory = IoC.resolve<ScreenFragmentFactory>("ScreenFragmentFactory")

    fun openScreen(screen: Screen) {
        fragmentManager.beginTransaction()
            .replace(containerId, factory.createScreen(screen))
            .commit()
    }
}