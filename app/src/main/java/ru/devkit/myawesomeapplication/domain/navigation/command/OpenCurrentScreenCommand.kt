package ru.devkit.myawesomeapplication.domain.navigation.command

import android.app.Activity
import ru.devkit.myawesomeapplication.di.IoC
import ru.devkit.myawesomeapplication.domain.command.ICommand
import ru.devkit.myawesomeapplication.domain.navigation.Navigator
import ru.devkit.myawesomeapplication.domain.navigation.Screen
import ru.devkit.myawesomeapplication.domain.navigation.ScreenStack

class OpenCurrentScreenCommand : ICommand {
    override fun execute() {
        val stack = IoC.resolve<ScreenStack>("ScreenStack")
        if (stack.isEmpty()) {
            ChangeScreenCommand(Screen.PortfolioScreen).execute()
        } else {
            IoC.resolve<Navigator>("Navigator").openScreen(stack.currentScreen())
        }
    }
}