package ru.devkit.myawesomeapplication.domain.navigation.command

import ru.devkit.myawesomeapplication.di.IoC
import ru.devkit.myawesomeapplication.domain.command.ICommand
import ru.devkit.myawesomeapplication.domain.navigation.Screen
import ru.devkit.myawesomeapplication.domain.navigation.ScreenStack

class ChangeScreenCommand(private val screen: Screen) : ICommand {
    override fun execute() {
        IoC.resolve<ScreenStack>("ScreenStack").push(screen)
        OpenCurrentScreenCommand().execute()
    }
}