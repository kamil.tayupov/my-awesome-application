package ru.devkit.myawesomeapplication.domain.navigation

sealed class Screen {
    data object PortfolioScreen : Screen()
    data class InvestmentScreen(val id: String) : Screen()
    data object SummaryScreen : Screen()
}