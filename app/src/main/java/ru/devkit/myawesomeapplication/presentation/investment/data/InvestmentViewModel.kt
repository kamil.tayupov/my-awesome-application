package ru.devkit.myawesomeapplication.presentation.investment.data

import androidx.lifecycle.LifecycleCoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import ru.devkit.myawesomeapplication.data.source.StockDataSource
import ru.devkit.myawesomeapplication.di.IoC
import ru.devkit.myawesomeapplication.domain.PortfolioRepository
import ru.devkit.myawesomeapplication.presentation.investment.model.InvestmentUiModel
import ru.devkit.myawesomeapplication.presentation.mapper.toUiModel

class InvestmentViewModel(id: String) {

    private val lifecycleScope = IoC.resolve<LifecycleCoroutineScope>("LifecycleCoroutineScope")
    private val repository = IoC.resolve<PortfolioRepository>("PortfolioRepository")
    private val stockDataSource = IoC.resolve<StockDataSource>("StockDataSource")

    private val _model = MutableStateFlow(InvestmentUiModel.EMPTY)
    val model = _model.asStateFlow()

    private val _ticks = MutableStateFlow(emptyList<Double>())
    val ticks = _ticks.asStateFlow()

    init {
        lifecycleScope.launch {
            repository.data.collect {
                val investment = it.investments.find { investment -> investment.id == id } ?: return@collect
                _model.value = investment.toUiModel()
                _ticks.value = stockDataSource.getStockHistory(id)
            }
        }
    }
}