package ru.devkit.myawesomeapplication.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import ru.devkit.myawesomeapplication.CoreApplication
import ru.devkit.myawesomeapplication.di.IoC

abstract class ScreenFragment: Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        IoC.resolve<CoreApplication>("CoreApplication").setScope(lifecycleScope)
    }
}