package ru.devkit.myawesomeapplication.presentation.investment.model

import ru.devkit.myawesomeapplication.presentation.portfolio.model.ListItemUiModel

data class InvestmentUiModel(
    val name: String,
    val value: String,
    val diffValue: String,
    val diffPercentage: String,
    val diffSign: ListItemUiModel.DiffSign
) {
    companion object {
        val EMPTY = InvestmentUiModel(
            name = "",
            value = "",
            diffValue = "",
            diffPercentage = "",
            diffSign = ListItemUiModel.DiffSign.NONE
        )
    }
}
