package ru.devkit.myawesomeapplication.presentation.investment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import ru.devkit.myawesomeapplication.databinding.FragmentInvestmentBinding
import ru.devkit.myawesomeapplication.di.IoC
import ru.devkit.myawesomeapplication.presentation.ScreenFragment
import ru.devkit.myawesomeapplication.presentation.investment.data.InvestmentViewModel
import ru.devkit.myawesomeapplication.presentation.investment.model.InvestmentUiModel
import ru.devkit.myawesomeapplication.presentation.ui.ChartMarkerView
import ru.devkit.myawesomeapplication.presentation.ui.ListItemComponent
import ru.devkit.myawesomeapplication.presentation.utils.getColorResCompat

import com.google.android.material.R as designR

private const val ARG_PARAM_ID = "param_id"

class InvestmentFragment private constructor() : ScreenFragment() {

    private lateinit var binding: FragmentInvestmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentInvestmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupChart()
    }

    override fun onResume() {
        super.onResume()
        setupDataUpdate()
    }

    private fun setupDataUpdate() {
        val id = arguments?.getString(ARG_PARAM_ID) ?: throw IllegalArgumentException("ID parameter is missing")
        val viewModel = IoC.resolve<InvestmentViewModel>("InvestmentViewModel", id)
        lifecycleScope.launch {
            viewModel.model.collectLatest(::updateData)
        }
        lifecycleScope.launch {
            viewModel.ticks.collectLatest(::updateTicks)
        }
    }

    private fun updateData(model: InvestmentUiModel) {
        binding.investmentTitle.setName(model.name)
        binding.investmentTitle.setData(
            ListItemComponent.Data(
                value = model.value,
                diffValue = model.diffValue,
                diffPercentage = model.diffPercentage,
                diffColor = model.diffSign.color
            )
        )
    }

    private fun updateTicks(ticks: List<Double>) {
        val entries = ticks.mapIndexed { index, value -> Entry(index.toFloat(), value.toFloat()) }
        binding.investmentChart.run {
            data = LineData().apply {
                addDataSet(createDataSet(entries))
                moveViewToX(entryCount.toFloat())
                setVisibleXRangeMaximum(20f)
                invalidate()
            }
        }
    }

    private fun createDataSet(entries: List<Entry>): LineDataSet {
        return LineDataSet(entries, "").apply {
            color = requireContext().getColor(designR.color.design_default_color_secondary)
            setCircleColor(color)
            setDrawValues(false)
            lineWidth = 2f
            circleRadius = 4f
            highlightLineWidth = 2f
            enableDashedHighlightLine(2f, 2f, 0f)
            mode = LineDataSet.Mode.HORIZONTAL_BEZIER
        }
    }

    private fun setupChart() {
        binding.investmentChart.apply {
            isScaleYEnabled = false
            legend.isEnabled = false
            description.isEnabled = false
            xAxis.position = XAxis.XAxisPosition.BOTTOM
            val textColor = context.getColorResCompat(android.R.attr.textColorSecondary)
            xAxis.textColor = textColor
            axisLeft.textColor = textColor
            axisRight.textColor = textColor
            ChartMarkerView(requireContext()).setChart(this)
            animateX(500)
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param id Investment ID.
         * @return A new instance of fragment investmentFragment.
         */
        @JvmStatic
        fun newInstance(id: String) =
            InvestmentFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM_ID, id)
                }
            }
    }
}