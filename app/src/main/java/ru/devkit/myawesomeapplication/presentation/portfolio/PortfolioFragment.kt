package ru.devkit.myawesomeapplication.presentation.portfolio

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import kotlinx.coroutines.launch
import ru.devkit.myawesomeapplication.databinding.FragmentPortfolioBinding
import ru.devkit.myawesomeapplication.di.IoC
import ru.devkit.myawesomeapplication.domain.navigation.Screen
import ru.devkit.myawesomeapplication.domain.navigation.command.ChangeScreenCommand
import ru.devkit.myawesomeapplication.presentation.ScreenFragment
import ru.devkit.myawesomeapplication.presentation.portfolio.adapter.InvestmentsAdapter
import ru.devkit.myawesomeapplication.presentation.portfolio.data.PortfolioViewModel
import ru.devkit.myawesomeapplication.presentation.portfolio.model.PortfolioUiModel
import ru.devkit.myawesomeapplication.presentation.ui.ListItemComponent

class PortfolioFragment private constructor() : ScreenFragment() {

    private lateinit var binding: FragmentPortfolioBinding
    private lateinit var investmentsAdapter: InvestmentsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentPortfolioBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupDataUpdate()
        setupNavigation()
    }

    private fun setupRecyclerView() {
        investmentsAdapter = InvestmentsAdapter()
        binding.investmentRecyclerView.apply {
            adapter = investmentsAdapter
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }
    }

    private fun setupDataUpdate() {
        val viewModel = IoC.resolve<PortfolioViewModel>("PortfolioViewModel")
        lifecycleScope.launch {
            viewModel.model.collect(::updateData)
        }
    }

    private fun updateData(model: PortfolioUiModel) {
        investmentsAdapter.submitList(model.items)
        binding.portfolioTitle.setData(
            ListItemComponent.Data(
                value = model.totalValue,
                diffValue = model.totalDiffValue,
                diffPercentage = model.totalDiffPercentage,
                diffColor = model.totalDiffSign.color
            )
        )
    }

    private fun setupNavigation() {
        binding.portfolioSummaryButton.setOnClickListener {
            ChangeScreenCommand(Screen.SummaryScreen).execute()
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment PortfolioFragment.
         */
        @JvmStatic
        fun newInstance() = PortfolioFragment()
    }
}