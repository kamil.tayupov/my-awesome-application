package ru.devkit.myawesomeapplication.presentation.summary.data

import androidx.lifecycle.LifecycleCoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import ru.devkit.myawesomeapplication.di.IoC
import ru.devkit.myawesomeapplication.domain.PortfolioRepository
import ru.devkit.myawesomeapplication.presentation.mapper.toSummaryUiModel
import ru.devkit.myawesomeapplication.presentation.summary.model.SummaryUiModel

class SummaryViewModel {

    private val lifecycleScope = IoC.resolve<LifecycleCoroutineScope>("LifecycleCoroutineScope")
    private val repository = IoC.resolve<PortfolioRepository>("PortfolioRepository")

    private val _model = MutableStateFlow(SummaryUiModel.EMPTY)
    val model = _model.asStateFlow()

    init {
        lifecycleScope.launch {
            repository.data.collect {
                _model.value = it.toSummaryUiModel()
            }
        }
    }
}