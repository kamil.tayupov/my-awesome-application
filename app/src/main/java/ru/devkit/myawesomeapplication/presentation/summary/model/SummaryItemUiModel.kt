package ru.devkit.myawesomeapplication.presentation.summary.model

data class SummaryItemUiModel(
    val id: String,
    val name: String,
    val value: String,
    val percentage: Float,
    val percentageString: String
)