package ru.devkit.myawesomeapplication.presentation

import ru.devkit.myawesomeapplication.domain.navigation.Screen
import ru.devkit.myawesomeapplication.presentation.investment.InvestmentFragment
import ru.devkit.myawesomeapplication.presentation.portfolio.PortfolioFragment
import ru.devkit.myawesomeapplication.presentation.summary.SummaryFragment

class ScreenFragmentFactory {
    fun createScreen(screen: Screen): ScreenFragment {
        return when (screen) {
            is Screen.PortfolioScreen -> PortfolioFragment.newInstance()
            is Screen.InvestmentScreen -> InvestmentFragment.newInstance(screen.id)
            is Screen.SummaryScreen -> SummaryFragment.newInstance()
        }
    }
}