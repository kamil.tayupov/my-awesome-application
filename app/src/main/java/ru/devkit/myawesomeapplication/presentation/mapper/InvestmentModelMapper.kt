package ru.devkit.myawesomeapplication.presentation.mapper

import ru.devkit.myawesomeapplication.domain.model.Investment
import ru.devkit.myawesomeapplication.domain.model.Portfolio
import ru.devkit.myawesomeapplication.domain.utils.formatCurrencyPrice
import ru.devkit.myawesomeapplication.domain.utils.formatPercentage
import ru.devkit.myawesomeapplication.presentation.investment.model.InvestmentUiModel
import ru.devkit.myawesomeapplication.presentation.portfolio.model.ListItemUiModel
import ru.devkit.myawesomeapplication.presentation.portfolio.model.PortfolioUiModel
import ru.devkit.myawesomeapplication.presentation.summary.model.SummaryItemUiModel
import ru.devkit.myawesomeapplication.presentation.summary.model.SummaryUiModel

fun Portfolio.toUiModel(): PortfolioUiModel {
    val items = investments.map {
        ListItemUiModel(
            id = it.id,
            name = it.name,
            value = formatCurrencyPrice(it.value),
            diffValue = formatCurrencyPrice(it.difference),
            diffPercentage = formatPercentage(it.difference / it.initialValue * 100),
            diffSign = ListItemUiModel.DiffSign.get(it.difference),
            lastSign = ListItemUiModel.LastSign.get(it.lastDifference)
        )
    }
    return PortfolioUiModel(
        items = items,
        totalValue = formatCurrencyPrice(totalValue),
        totalDiffValue = formatCurrencyPrice(totalDifference),
        totalDiffPercentage = formatPercentage(totalDifference / totalInitialValue * 100),
        totalDiffSign = ListItemUiModel.DiffSign.get(totalDifference),
    )
}

fun Investment.toUiModel(): InvestmentUiModel {
    return InvestmentUiModel(
        name = name,
        value = formatCurrencyPrice(value),
        diffValue = formatCurrencyPrice(difference),
        diffPercentage = formatPercentage(difference / initialValue * 100),
        diffSign = ListItemUiModel.DiffSign.get(difference)
    )
}

fun Portfolio.toSummaryUiModel(): SummaryUiModel {
    val items = investments.map {
        val percentage = it.value / totalValue
        SummaryItemUiModel(
            id = it.id,
            name = it.name,
            value = formatCurrencyPrice(it.value),
            percentage = (it.value / totalValue).toFloat(),
            percentageString = formatPercentage(percentage * 100)
        )
    }
    return SummaryUiModel(
        items = items,
        totalValue = formatCurrencyPrice(totalValue)
    )
}