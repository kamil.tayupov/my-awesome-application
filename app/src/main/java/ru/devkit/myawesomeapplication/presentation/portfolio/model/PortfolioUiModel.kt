package ru.devkit.myawesomeapplication.presentation.portfolio.model

data class PortfolioUiModel(
    val items: List<ListItemUiModel>,
    val totalValue: String,
    val totalDiffValue: String,
    val totalDiffPercentage: String,
    val totalDiffSign: ListItemUiModel.DiffSign
) {
    companion object {
        val EMPTY = PortfolioUiModel(
            items = emptyList(),
            totalValue = "",
            totalDiffValue = "",
            totalDiffPercentage = "",
            totalDiffSign = ListItemUiModel.DiffSign.NONE
        )
    }
}