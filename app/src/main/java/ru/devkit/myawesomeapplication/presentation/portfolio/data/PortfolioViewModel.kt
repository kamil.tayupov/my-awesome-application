package ru.devkit.myawesomeapplication.presentation.portfolio.data

import androidx.lifecycle.LifecycleCoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import ru.devkit.myawesomeapplication.di.IoC
import ru.devkit.myawesomeapplication.domain.PortfolioRepository
import ru.devkit.myawesomeapplication.presentation.mapper.toUiModel
import ru.devkit.myawesomeapplication.presentation.portfolio.model.PortfolioUiModel

class PortfolioViewModel {

    private val lifecycleScope = IoC.resolve<LifecycleCoroutineScope>("LifecycleCoroutineScope")
    private val repository = IoC.resolve<PortfolioRepository>("PortfolioRepository")

    private val _model = MutableStateFlow(PortfolioUiModel.EMPTY)
    val model = _model.asStateFlow()

    init {
        lifecycleScope.launch {
            repository.data.collect {
                _model.value = it.toUiModel()
            }
        }
    }
}