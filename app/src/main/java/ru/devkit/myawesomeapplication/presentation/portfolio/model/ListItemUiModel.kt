package ru.devkit.myawesomeapplication.presentation.portfolio.model

import android.graphics.Color
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import ru.devkit.myawesomeapplication.R
import ru.devkit.myawesomeapplication.domain.utils.isNegative
import ru.devkit.myawesomeapplication.domain.utils.isPositive

const val ARG_VALUE = "arg_value"
const val ARG_DIFF_VALUE = "arg_diff_value"
const val ARG_DIFF_PERCENTAGE = "arg_diff_percentage"
const val ARG_DIFF_SIGN = "arg_diff_sign"
const val ARG_LAST_SIGN = "arg_last_sign"

data class ListItemUiModel(
    val id: String,
    val name: String,
    val value: String,
    val diffValue: String,
    val diffPercentage: String,
    val diffSign: DiffSign,
    val lastSign: LastSign
) {
    enum class DiffSign(@ColorInt val color: Int) {
        PLUS(Color.GREEN),
        MINUS(Color.RED),
        NONE(Color.BLACK);

        companion object {
            fun get(value: Double): DiffSign {
                return when {
                    value.isNegative() -> MINUS
                    value.isPositive() -> PLUS
                    else -> NONE
                }
            }
        }
    }

    enum class LastSign(@DrawableRes val icon: Int) {
        PLUS(R.drawable.ic_arrow_up),
        MINUS(R.drawable.ic_arrow_down),
        NONE(R.drawable.ic_empty);

        companion object {
            fun get(value: Double): LastSign {
                return when {
                    value.isNegative() -> MINUS
                    value.isPositive() -> PLUS
                    else -> NONE
                }
            }
        }
    }
}