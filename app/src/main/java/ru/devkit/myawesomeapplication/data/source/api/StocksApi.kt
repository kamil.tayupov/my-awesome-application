package ru.devkit.myawesomeapplication.data.source.api

import ru.devkit.myawesomeapplication.data.model.StockDto

interface StocksApi {
    fun getStock(id: String): StockDto
    fun getStockHistory(id: String): List<Double>
    fun release()
}