package ru.devkit.myawesomeapplication.data.source.api

import kotlinx.coroutines.flow.Flow
import ru.devkit.myawesomeapplication.data.model.PortfolioDto

interface PortfolioApi {
    fun getPortfolio(): Flow<PortfolioDto>
    fun release()
}