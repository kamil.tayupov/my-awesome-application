package ru.devkit.myawesomeapplication.data.source.mock

data class MockDto(
    val id: String,
    val name: String,
    val quantity: Int,
    val price: Double
)