package ru.devkit.myawesomeapplication.data.model

data class PortfolioDto(
    val items: List<InvestmentDto>
)