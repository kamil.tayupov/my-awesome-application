package ru.devkit.myawesomeapplication.data.source.mock

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import ru.devkit.myawesomeapplication.data.model.InvestmentDto
import ru.devkit.myawesomeapplication.data.model.PortfolioDto
import ru.devkit.myawesomeapplication.data.source.api.PortfolioApi
import ru.devkit.myawesomeapplication.di.IoC
import java.util.concurrent.atomic.AtomicReference

class MockPortfolioService : PortfolioApi {

    private val commonService = IoC.resolve<MockCommonService>("MockCommonService")

    private var data = AtomicReference(PortfolioDto(emptyList()))

    private var isRunning = true

    init {
        data.set(
            PortfolioDto(
                items = commonService.dto.map {
                    InvestmentDto(
                        id = it.id,
                        quantity = it.quantity,
                        initialPrice = it.price
                    )
                }
            )
        )
    }

    override fun release() {
        isRunning = false
    }

    override fun getPortfolio(): Flow<PortfolioDto> = flow {
        while (isRunning) {
            emit(data.get())
            delay(1_000)
        }
    }
}