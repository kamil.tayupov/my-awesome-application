package ru.devkit.myawesomeapplication.data.source

import ru.devkit.myawesomeapplication.data.model.StockDto
import ru.devkit.myawesomeapplication.data.source.api.StocksApi
import ru.devkit.myawesomeapplication.di.IoC

class StockDataSource {

    private val api = IoC.resolve<StocksApi>("StocksApi")

    fun getStock(id: String): StockDto = api.getStock(id)

    fun getStockHistory(id: String): List<Double> = api.getStockHistory(id)
}