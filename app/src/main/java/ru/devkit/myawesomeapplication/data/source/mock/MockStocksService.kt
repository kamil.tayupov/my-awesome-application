package ru.devkit.myawesomeapplication.data.source.mock

import ru.devkit.myawesomeapplication.data.model.StockDto
import ru.devkit.myawesomeapplication.data.source.api.StocksApi
import ru.devkit.myawesomeapplication.di.IoC
import java.util.concurrent.atomic.AtomicReference
import kotlin.concurrent.thread
import kotlin.random.Random

class MockStocksService : StocksApi {

    private val commonService = IoC.resolve<MockCommonService>("MockCommonService")

    private val cache = AtomicReference(mutableMapOf<String, List<Double>>())

    private var isRunning = true

    init {
        thread {
            while (isRunning) {
                val update = cache.get().mapValues { (_, value) ->
                    if (Random.nextInt(5) == 0) {
                        val last = value.last()
                        value + (last + offset(last))
                    } else {
                        value
                    }
                }
                cache.set(update.toMutableMap())
                Thread.sleep(1_000)
            }
        }
    }

    override fun getStock(id: String): StockDto {
        val stock = findDto(id)
        val history = getStockHistory(id)
        return StockDto(
            id = stock.id,
            name = stock.name,
            currentPrice = history.last(),
            lastPrice = history.dropLast(1).last()
        )
    }

    override fun getStockHistory(id: String): List<Double> {
        val history = cache.get()
        if (history.containsKey(id).not()) {
            val first = findDto(id).price
            history[id] = List(20) { first + offset(first) }
        }
        return history[id] ?: throw IllegalArgumentException("ID $id was not found")
    }

    override fun release() {
        isRunning = false
    }

    private fun findDto(id: String): MockDto {
        return commonService.dto.find { it.id == id } ?: throw IllegalArgumentException("ID $id was not found")
    }

    private fun offset(value: Double) = Random.nextDouble(-value * 0.1, value * 0.1)
}