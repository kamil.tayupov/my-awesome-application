package ru.devkit.myawesomeapplication.data.source

import kotlinx.coroutines.flow.Flow
import ru.devkit.myawesomeapplication.data.model.PortfolioDto
import ru.devkit.myawesomeapplication.data.source.api.PortfolioApi
import ru.devkit.myawesomeapplication.di.IoC

class PortfolioRemoteDataSource {

    private val api = IoC.resolve<PortfolioApi>("PortfolioApi")

    fun getData(): Flow<PortfolioDto> = api.getPortfolio()
}