package ru.devkit.myawesomeapplication.data.model

data class InvestmentDto(
    val id: String,
    val quantity: Int,
    val initialPrice: Double
)