package ru.devkit.myawesomeapplication.data.model

data class StockDto(
    val id: String,
    val name: String,
    val currentPrice: Double,
    val lastPrice: Double
)