package ru.devkit.myawesomeapplication.data.source

import ru.devkit.myawesomeapplication.domain.model.Portfolio
import java.util.concurrent.atomic.AtomicReference

class PortfolioLocalDataSource {

    private var data = AtomicReference(Portfolio(emptyList()))

    fun getData(): Portfolio {
        return data.get()
    }

    fun setData(data: Portfolio) {
        this.data.set(data)
    }
}