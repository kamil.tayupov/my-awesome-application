package ru.devkit.myawesomeapplication.data.source.mock

import com.google.gson.Gson
import ru.devkit.myawesomeapplication.CoreApplication
import ru.devkit.myawesomeapplication.R
import ru.devkit.myawesomeapplication.data.model.StockDto
import ru.devkit.myawesomeapplication.di.IoC
import java.io.BufferedReader
import java.io.InputStreamReader

class MockCommonService {

    private val context = IoC.resolve<CoreApplication>("CoreApplication").applicationContext

    internal val dto = getDto()

    private fun getDto(): List<MockDto> {
        try {
            val inputStream = context.resources.openRawResource(R.raw.stocks)
            val inputStreamReader = InputStreamReader(inputStream)
            val bufferedReader = BufferedReader(inputStreamReader)
            val stringBuilder = StringBuilder()

            var line: String?
            while (bufferedReader.readLine().also { line = it } != null) {
                stringBuilder.append(line)
            }

            val jsonString = stringBuilder.toString()
            return Gson().fromJson(jsonString, Array<MockDto>::class.java).toList()
        } catch (e: Exception) {
            e.printStackTrace()
            return emptyList()
        }
    }
}