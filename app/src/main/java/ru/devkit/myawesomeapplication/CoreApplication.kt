package ru.devkit.myawesomeapplication

import android.app.Application
import androidx.lifecycle.LifecycleCoroutineScope
import ru.devkit.myawesomeapplication.data.source.PortfolioLocalDataSource
import ru.devkit.myawesomeapplication.data.source.PortfolioRemoteDataSource
import ru.devkit.myawesomeapplication.data.source.StockDataSource
import ru.devkit.myawesomeapplication.data.source.mock.MockCommonService
import ru.devkit.myawesomeapplication.data.source.mock.MockPortfolioService
import ru.devkit.myawesomeapplication.data.source.mock.MockStocksService
import ru.devkit.myawesomeapplication.di.IoC
import ru.devkit.myawesomeapplication.di.strategy.NewInstanceStrategy
import ru.devkit.myawesomeapplication.di.strategy.SingleInstanceStrategy
import ru.devkit.myawesomeapplication.domain.PortfolioRepository
import ru.devkit.myawesomeapplication.domain.navigation.ScreenStack
import ru.devkit.myawesomeapplication.presentation.ScreenFragmentFactory
import ru.devkit.myawesomeapplication.presentation.investment.data.InvestmentViewModel
import ru.devkit.myawesomeapplication.presentation.portfolio.data.PortfolioViewModel
import ru.devkit.myawesomeapplication.presentation.summary.data.SummaryViewModel

class CoreApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        // Core
        IoC.register("CoreApplication", SingleInstanceStrategy { this })

        // Apis
        IoC.register("PortfolioApi", SingleInstanceStrategy { MockPortfolioService() })
        IoC.register("StocksApi", SingleInstanceStrategy { MockStocksService() })
        IoC.register("MockCommonService", SingleInstanceStrategy { MockCommonService() })

        // Data Sources
        IoC.register("PortfolioLocalDataSource", SingleInstanceStrategy { PortfolioLocalDataSource() })
        IoC.register("PortfolioRemoteDataSource", SingleInstanceStrategy { PortfolioRemoteDataSource() })
        IoC.register("StockDataSource", SingleInstanceStrategy { StockDataSource() })

        // Repositories
        IoC.register("PortfolioRepository", SingleInstanceStrategy { PortfolioRepository() })

        // View Models
        IoC.register("PortfolioViewModel", NewInstanceStrategy { PortfolioViewModel() })
        IoC.register("InvestmentViewModel", NewInstanceStrategy { args -> InvestmentViewModel(args[0] as String) })
        IoC.register("SummaryViewModel", NewInstanceStrategy { SummaryViewModel() })

        // Screens
        IoC.register("ScreenFragmentFactory", NewInstanceStrategy { ScreenFragmentFactory() })
        IoC.register("ScreenStack", SingleInstanceStrategy { ScreenStack() })
    }

    fun setScope(lifecycleScope: LifecycleCoroutineScope) {
        IoC.register("LifecycleCoroutineScope", SingleInstanceStrategy { lifecycleScope })
    }
}