package ru.devkit.myawesomeapplication

import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import ru.devkit.myawesomeapplication.di.IoC
import ru.devkit.myawesomeapplication.di.strategy.SingleInstanceStrategy
import ru.devkit.myawesomeapplication.domain.navigation.Navigator
import ru.devkit.myawesomeapplication.domain.navigation.command.OnBackScreenCommand
import ru.devkit.myawesomeapplication.domain.navigation.command.OpenCurrentScreenCommand

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initialize()
    }

    override fun onResume() {
        super.onResume()
        OpenCurrentScreenCommand().execute()
    }

    private fun initialize() {
        IoC.register("Activity", SingleInstanceStrategy { this })
        IoC.register("Navigator", SingleInstanceStrategy {
            Navigator(supportFragmentManager, R.id.fragment_container)
        })
        interceptOnBackPressed()
    }

    private fun interceptOnBackPressed() {
        onBackPressedDispatcher.addCallback(object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                OnBackScreenCommand().execute()
            }
        })
    }
}